<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public $title;
	public $content;
	public $date;

	public function get_last_ten_users()
	{
			$query = $this->db->get('users', 10);
			return $query->result();
	}

	public function insert_entry()
	{
			$this->title    = $_POST['title']; // please read the below note
			$this->content  = $_POST['content'];
			$this->date     = time();

			$this->db->insert('users', $this);
	}

	public function update_entry()
	{
			$this->title    = $_POST['title'];
			$this->content  = $_POST['content'];
			$this->date     = time();

			$this->db->update('users', $this, array('id' => $_POST['id']));
	}

	public function delete_entry()
	{
		$this->db->delete('users', array('id' => $id));
}


